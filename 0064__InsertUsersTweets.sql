USE ModernWays;
INSERT INTO Users (Handle)
VALUES
('@NintendoEurope'),
('@Xbox');
INSERT INTO Tweets (Bericht, Tweet_Id)
VALUES
("Don't forget -- Nintendo Labo: VR Kit launches 12/04!"),
("Splat it out in the #Splatoon2 EU Community Cup 5 this Sunday!"),
("Crikey! Keep an eye out for cardboard crocs and other crafty wildlife on this jungle train ride! #Yoshi"),
("You had a lot to say about #MetroExodus. Check out our favorite 5-word reviews."),
("It's a perfect day for some mayhem."),
("Drift all over N. Sanity Beach and beyond in Crash Team Racing Nitro-Fueled.");
