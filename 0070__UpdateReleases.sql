USE ModernWays;
SELECT Games.Titel, Platformen.Naam, Date
FROM Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
INNER JOIN Games ON Releases.Games_Id = Games.Id
