USE ModernWays;
ALTER TABLE Users
ADD COLUMN Tweets_Id INT,
ADD CONSTRAINT fk_Users_Tweets
FOREIGN KEY (Tweet_Id)
REFERENCES Tweets(TweetID);
