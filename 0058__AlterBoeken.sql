ALTER TABLE Boeken
ADD COLUMN Personen_Id INT,
ADD CONSTRAINT fk_Boeken_fk_Personen
  FOREIGN KEY (Personen_Id)
  REFERENCES Personen(Id);